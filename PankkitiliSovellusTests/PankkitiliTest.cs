﻿using NUnit.Framework;
using PankkitiliSovellus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkitiliSovellusTests
{
    [TestFixture]
    public class PankkitiliTest
    {
        /*PankkitiliSovellus sisältää:
         * 
         * Luokka, jonka nimi on Pankkitili.
         * Pankkitilissä on kolme toimintoa.
         * 
         * - Tallettaa rahaa
         * - Nostaa rahaa
         * --Tarkistaa ettei tili mene miinukselle
         * - Siirtää rahaa tililtä toiselle
         * --Tarkistaa ettei tili mene miinukselle
         *
         */

        public Pankkitili tili1 = null;

        // OneTimeSetUp
        // OneTimeTearDown
        // TearDown
        [SetUp]
        public void TestienAlustaja()
        {
            this.tili1 = new Pankkitili(100);
        }
        

        [Test]
        public void LuoPankkitili()
        {           
            // Testataan olion luokan tyyppi
            Assert.IsInstanceOf<Pankkitili>(tili1);
        }

        [Test]
        public void  AsetaPankkitililleAlkusaldo()
        {
            // Testataan arvon yhtäsuuruutta
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void TalletaRahaaPankkitilille()
        {            
            // Talletetaan rahaa tilille
            tili1.Talleta(250);

            // Testataan arvon yhtäsuuruutta
            Assert.That(350, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostaPankkitililtäRahaa()
        {
            // Arrange
            // Pankkitili tili1 = new Pankkitili (100)

            // act

            tili1.NostaRahaa(75);

            // Assert
            // Testataan arvon yhtäsuuruutta
            Assert.That(25, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostonJalkeenPankkitiliEiVoiOllaMiinuksella()
        {
            
            // Testataan antaako ohjelma halutun virhetyypin
            Assert.Throws<ArgumentException>(()=>tili1.NostaRahaa(175));
            // Vaikka virhe sattuu niin rahoja ei menetä.
            // Pitäisi olla loppusaldon kanssa sama.
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [TestCase(450, 200, 250)]
        public void NostaPankkitililtäRahaa2(int alkusaldo, int nostoMaara, int saldo)
        {
            Pankkitili tili1 = new Pankkitili(alkusaldo);

            tili1.NostaRahaa(nostoMaara);

          
            Assert.That(saldo, Is.EqualTo(tili1.Saldo));
        }

    }
}
